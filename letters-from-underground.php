<?php
/*
Plugin Name: Letters from Underground
Plugin URI: http://frankie.winters.design/letters
Version: 0.2
Author: Frankie
Description: Some ink for the typewriter.
Text Domain: letters-from-underground
License: BSD 3-Clause
*/

/**
 * Register jquery and style on initialization
 */
function letters_register_script() {
    wp_register_style( 'letters-from-underground', plugins_url('/letters-from-underground.css', __FILE__), false, '1.0.0', 'all');
}

add_action('init', 'letters_register_script');

/**
 * Include our styles and morphext
 */
function letters_enqueue_style(){
  wp_enqueue_script( 'morphext', plugins_url('/vendor/morphext.min.js', __FILE__), array( 'jquery' ) );
  wp_enqueue_style('letters-from-underground-morphext', plugins_url('/vendor/morphext.css', __FILE__), array(), '2.0');
  wp_enqueue_script( 'letters-from-underground-activate-morphext', plugins_url('activate-morphext.js', __FILE__), array( 'jquery' ) );
   // Letters from Underground
  //  if ( is_home() || is_singular( 'letter' )  || is_single('letters-from-underground') ) {
     wp_enqueue_style('letters-from-underground');
  //  }
}

add_action('wp_enqueue_scripts', 'letters_enqueue_style', 40);

/**
 * Prepend a link to the project page on Letters from Underground
 */
add_filter( 'the_content', 'wintersdesign_lettersfomunderground' );

function wintersdesign_lettersfomunderground( $content ) {
   if ( is_singular( 'letter' ) ) {
		$index_link = '<p style="text-align: center"><a class="letters-home-link" href="/portfolio/letters-from-underground/">Letters from Underground</a></p>';
		$content .= $index_link;
	}
   return $content;
}

/**
 * A count of category-letters-from-underground
 */
function wintersdesign_count_lettersfromunderground() {
	$args = array(
	    'posts_per_page' => 500,
      'post_type' => 'letter',
	);
	$the_query = new WP_Query( $args );
	return $the_query->found_posts;
}

add_shortcode( 'letters_count', 'wintersdesign_count_lettersfromunderground' );

/**
 * Register Letters Post Type
 */
 function create_letters_from_underground_cpt() {

	$labels = array(
		'name'                  => _x( 'Letters', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Letter', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Letters', 'text_domain' ),
		'name_admin_bar'        => __( 'Letters from Underground', 'text_domain' ),
		'archives'              => __( 'Letters From Underground Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Letters', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Letter', 'text_domain' ),
		'edit_item'             => __( 'Edit Letter', 'text_domain' ),
		'update_item'           => __( 'Update Letter', 'text_domain' ),
		'view_item'             => __( 'View Letter', 'text_domain' ),
		'view_items'            => __( 'View Letters', 'text_domain' ),
		'search_items'          => __( 'Search Letter', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Letter', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Letter', 'text_domain' ),
		'items_list'            => __( 'Letters list', 'text_domain' ),
		'items_list_navigation' => __( 'Letters navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter letters list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Letter', 'text_domain' ),
		'description'           => __( 'A work of nonlinear, speculative, epistolary, dystopian fiction in letters_count parts.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-welcome-comments',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'letter', $args );

}
add_action( 'init', 'create_letters_from_underground_cpt', 0 );
